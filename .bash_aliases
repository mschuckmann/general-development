alias ipconfig='ifconfig'
#function e() { /home/mschuckmann/eclipse/cpp-mars/eclipse/eclipse "$@" &> /dev/null ;}
#alias a='atom'
#alias e='code -r'
function e() { 
   case $# in
   0)
      code -r
      ;;
   1)
      code -r "$1"
      ;;
   2)
      code -r "$1" -g "$2"
      ;;
   esac
} 
alias c='code -r -g'
   
alias t='wn-telnet.sh $WN_TARGET_DEVICE'
alias p='ping $WN_TARGET_DEVICE'
alias wut='makeweb.sh && wn-upload && sleep 35 && t'
alias wn-root='cd $BUILD_ROOT/linux/user/lantronix/planar'
alias wn-build='make -C $BUILD_ROOT/linux/user/lantronix/planar'
alias wn-upload='uploadfw.sh $WN_TARGET_DEVICE'
alias build-root='cd $BUILD_ROOT'
alias dev-root='cd $DEV_ROOT'
alias linux-root='cd $BUILD_ROOT/linux'
#tail something
alias tf='tail -f '
#grep in various source files
alias findcpp='find . -iname "*.cpp" | xargs grep -ni --color=always '
alias findc='find . -iname "*.c" | xargs grep -ni --color=always '
alias findh='find . -iname "*.h" | xargs grep -ni --color=always '
alias finds='find . -iname "README.*" -o -iname "*.css" -o -iname "*.cpp" -o -iname "*.c" -o -iname "*.h" -o -iname "*.py" -o -iname "*.bb" -o -iname "*.ppapend" -o -iname "*.hpp" -o -iname "*.sh" -o -iname "*.js" -o -iname "*.html" -o -iname "*.conf" | xargs grep -ni --color=always '
alias finda='find . -name "*" | xargs grep -ni --color=always 2> /dev/null'
alias findf='find . -iname 2> /dev/null'
#Base number conversions
function h2b() { echo "obase=2; ibase=16; $@" | bc ;}
function h2d() { echo "obase=10;ibase=16; $@" | bc ;}
function h2o() { echo "obase=8; ibase=16; $@" | bc ;}
function d2h() { echo "obase=16;ibase=10; $@" | bc ;}
function d2b() { echo "obase=2;ibase=10; $@" | bc ;}
function d2o() { echo "obase=8; ibase=10; $@" | bc ;}
function o2h() { echo "obase=16;ibase=8; $@" | bc ;}
function o2d() { echo "obase=10;ibase=8; $@" | bc ;}
function o2b() { echo "obase=2; ibase=8; $@" | bc ;}
function b2d() { echo "obase=10;ibase=2; $@" | bc ;}
function b2h() { echo "obase=16;ibase=2; $@" | bc ;}
function b2o() { echo "obase=8; ibase=2; $@" | bc ;}

#Make quietly
alias ms='make -s '
alias mj='make -j '
alias mjs='make -js '

alias miniusb0='minicom -D /dev/ttyUSB0'
alias miniusb1='minicom -D /dev/ttyUSB1'
alias miniusb2='minicom -D /dev/ttyUSB2'
